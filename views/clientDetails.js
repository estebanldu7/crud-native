import React from 'react';
import {StyleSheet, View, Alert, Platform} from 'react-native';
import {Headline, Text, Subheading, Button, FAB} from 'react-native-paper';
import axios from 'axios';

import globalStyles from '../styles/main';


const ClientDetails = ({route, navigation}) => {

    const {ruc, name, phone, address, email, id} = route.params.item;
    const {setGetApi} = route.params;

    const showConfirmation = () => {
        Alert.alert(
            'Estas seguro que deseas eliminar este cliente?',
            'Un contacto eliminado no se puede recuperar.',
            [
                {text: 'Eliminar', onPress: () => deleteClient()},
                {text: 'Cancelar', style: 'cancel'},
            ],
        );
    };

    const deleteClient = async () => {
        try {
            if (Platform.OS === 'ios') {
                await axios.delete(`http://localhost:8091/clients/${id}`);
            } else {
                await axios.delete(`http://10.0.2.2:8091/clients/${id}`);
            }
        } catch (e) {

        }

        navigation.navigate('Home');
        setGetApi(true);

    };

    return (
        <View style={globalStyles.content}>
            <Headline style={globalStyles.title}> {name} </Headline>
            <Text style={styles.text}><Subheading>Ruc: {ruc}</Subheading></Text>
            <Text style={styles.text}><Subheading>Teléfono: {phone}</Subheading></Text>
            <Text style={styles.text}><Subheading>Dirección: {address}</Subheading></Text>
            <Text style={styles.text}><Subheading>Email: {email}</Subheading></Text>

            <Button
                style={styles.button}
                mode="contained"
                icon="cancel"
                onPress={() => showConfirmation()}
            >
                Eliminar Cliente
            </Button>
            <FAB
                onPress={() => navigation.navigate('CreateClient', {client: route.params.item})}
                icon="pencil"
                style={globalStyles.fab}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    text: {
        marginBottom: 20,
        fontSize: 10,
    },
    button: {
        backgroundColor: 'red',
        marginTop: 80,
    },
});

export default ClientDetails;
