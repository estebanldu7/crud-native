import React, {useState, useEffect} from 'react';
import {View, Platform, FlatList} from 'react-native';
import {List, Headline, FAB} from 'react-native-paper';
import axios from 'axios';

import globalStyles from '../styles/main';

const Home = ({navigation}) => {

    const [clients, setClients] = useState([]);
    const [getApi, setGetApi] = useState(true);

    useEffect(() => {
        const getClients = async () => {
            try {
                let resp;

                if (Platform.OS === 'ios') {
                    resp = await axios.get('http://localhost:8091/clients');
                } else {
                    resp = await axios.get('http://10.0.2.2:8091/clients');
                }

                setClients(resp.data);
                setGetApi(false);
            } catch (error) {

            }
        };

        if (getApi) {
            getClients();
        }
    }, [getApi]);

    return (
        <View style={globalStyles.content}>
            <Headline style={globalStyles.title}>{clients.length > 0 ? 'Clientes' : 'No existen clientes'}</Headline>
            <FlatList
                data={clients}
                keyExtractor={client => (client.id).toString()}
                renderItem={({item}) => (
                    <List.Item
                        title={item.name}
                        description={item.ruc}
                        onPress={() => navigation.navigate('ClientDetails', {item, setGetApi})}
                    />
                )}/>
            <FAB
                onPress={() => navigation.navigate('CreateClient', {setGetApi})}
                icon="plus"
                style={globalStyles.fab}/>
        </View>
    );
};


export default Home;
