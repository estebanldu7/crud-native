import React, {useState, useEffect} from 'react';
import {TextInput, Headline, Button, Paragraph, Dialog, Portal} from 'react-native-paper';
import {StyleSheet, View, Platform} from 'react-native';
import axios from 'axios';

import globalStyles from '../styles/main';

const CreateClient = ({navigation, route}) => {

    const [ruc, setRuc] = useState('');
    const [name, setName] = useState('');
    const [phone, setPhone] = useState('');
    const [address, setAddress] = useState('');
    const [email, setEmail] = useState('');
    const [alert, setAlert] = useState(false);

    const {setGetApi} = route.params;

    useEffect(() => {
        if (route.params.client) {
            const {ruc, name, phone, address, email} = route.params.client;
            setRuc(ruc);
            setName(name);
            setPhone(phone);
            setAddress(address);
            setEmail(email);
        }
    }, []);

    const saveClient = async () => {
        const client = {ruc, name, phone, address, email};

        if (name === '' || ruc === '' || phone === '' || address === '' || email === '') {
            setAlert(true);
            return;
        }

        if (route.params.client) {
            const {id} = route.params.client;
            client.id = id;

            try {
                if (Platform.OS === 'ios') {
                    await axios.put(`http://localhost:8091/clients/${id}`, client);
                } else {
                    await axios.put(`http://10.0.2.2:8091/clients/${id}`, client);
                }
            } catch (e) {
                console.log('Error in update');
            }
        } else {
            try {
                if (Platform.OS === 'ios') {
                    await axios.post('http://localhost:8091/clients', client);
                } else {
                    await axios.post('http://10.0.2.2:8091/clients', client);
                }

            } catch (e) {
                console.log('Error in create');
            }
        }

        //Redirect
        navigation.navigate('Home');

        //Clean Form
        setRuc('');
        setName('');
        setPhone('');
        setAddress('');
        setEmail('');

        setGetApi(true);
    };

    return (
        <View style={globalStyles.content}>
            <Headline style={globalStyles.title}>Añadir Nuevo Cliente</Headline>
            <TextInput
                style={styles.input}
                label="RUC"
                value={ruc}
                onChangeText={text => setRuc(text)}
                placeholder="170101010101001"
            />
            <TextInput
                style={styles.input}
                label="Nombre Empresa"
                value={name}
                onChangeText={text => setName(text)}
                placeholder="Empresa Ejemplo"
            />
            <TextInput
                style={styles.input}
                label="Telefono"
                value={phone}
                onChangeText={text => setPhone(text)}
                placeholder="0229829202"
            />
            <TextInput
                style={styles.input}
                label="Direccion"
                value={address}
                onChangeText={text => setAddress(text)}
                placeholder="Av Amazonas"
            />
            <TextInput
                style={styles.input}
                label="Email"
                value={email}
                onChangeText={text => setEmail(text)}
                placeholder="admin@ejemplo.com"
            />
            <Button
                icon="pencil-circle"
                mode="contained"
                onPress={() => saveClient()}
            >Guardar Cliente
            </Button>

            <Portal>
                <Dialog
                    visible={alert}
                    onDismiss={() => setAlert(false)}
                >
                    <Dialog.Title>Error</Dialog.Title>
                    <Dialog.Content>
                        <Paragraph>Todos los campos son obligatorios.</Paragraph>
                    </Dialog.Content>
                    <Dialog.Actions>
                        <Button onPress={() => setAlert(false)}>OK</Button>
                    </Dialog.Actions>
                </Dialog>
            </Portal>
        </View>
    );
};

const styles = StyleSheet.create({
    input: {
        marginBottom: 20,
        backgroundColor: 'transparent',
    },
});

export default CreateClient;

