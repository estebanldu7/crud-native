import 'react-native-gesture-handler';
import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {DefaultTheme, Provider as PaperProvider} from 'react-native-paper';

import ClientDetails from './views/clientDetails';
import CreateClient from './views/createClient';
import Home from './views/home';
import Bar from './components/ui/bar';

const Stack = createStackNavigator();

const theme = {
    ...DefaultTheme,
    colors: {
        ...DefaultTheme.colors,
        primary: '#1774F2',
        accent: '#0655BF',
    },
};

const App = () => {
    return (
        <>
            <PaperProvider>
                <NavigationContainer>
                    <Stack.Navigator
                        initialRouteName="Home"
                        screenOptions={{
                            headerTitleAlign: 'center',
                            headerStyle: {
                                backgroundColor: theme.colors.primary,
                            },
                            headerTintColor: theme.colors.surface,
                            headerTitleStyle: {
                                fontWeight: 'bold',
                            },
                        }}
                    >
                        <Stack.Screen
                            name="Home"
                            component={Home}
                        />
                        <Stack.Screen
                            name="CreateClient"
                            component={CreateClient}
                            options={{title: 'Nuevo Cliente'}}
                        />
                        <Stack.Screen
                            name="ClientDetails"
                            component={ClientDetails}
                            options={{title: 'Detalles Cliente'}}
                        />
                    </Stack.Navigator>
                </NavigationContainer>
            </PaperProvider>
        </>
    );
};

const styles = StyleSheet.create({});

export default App;
