import React from 'react';
import {Button} from 'react-native-paper';

const Bar = ({navigation, route}) => {

    const handlePress = () => {
        navigation.navigate('CreateClient');
    };

    return (
        <Button icon="plus" color="#fff" onPress={() => handlePress()}>
            Nuevo
        </Button>
    );
};

export default Bar;
