import {StyleSheet} from 'react-native';

const globalStyles = StyleSheet.create({
    content: {
        flex: 1,
        marginTop: 20,
        marginHorizontal: '5%',
    },
    title: {
        textAlign: 'center',
        marginTop: 20,
        marginBottom: 30,
        fontSize: 30,
    },
    fab: {
        position: 'absolute',
        margin: 20,
        bottom: 20,
        right: 0,
    },
});

export default globalStyles;
